import logging
import pprint
import random
import time
from playsound import playsound

logging.basicConfig(level=logging.INFO, filename="blackjack.log")


def shop(counter):
    print("    "*2000)
    playsound("sounds/magic.mp3")
    # playsound("sounds/bunny.mp3")
    print("__    __    ___  _         __   ___   ___ ___    ___      ______   ___       ______  __ __    ___       _____ __ __   ___   ____  __  __  __ ")
    print("|  T__T  T  /  _]| T       /  ] /   \ |   T   T  /  _]    |      T /   \     |      T|  T  T  /  _]     / ___/|  T  T /   \ |    \|  T|  T|  T")
    print("|  |  |  | /  [_ | |      /  / Y     Y| _   _ | /  [_     |      |Y     Y    |      ||  l  | /  [_     (   \_ |  l  |Y     Y|  o  )  ||  ||  |")
    print("|  |  |  |Y    _]| l___  /  /  |  O  ||  \_/  |Y    _]    l_j  l_j|  O  |    l_j  l_j|  _  |Y    _]     \__  T|  _  ||  O  ||   _/|__j|__j|__j")
    print("l  `  '  !|   [_ |     T/   \_ |     ||   |   ||   [_       |  |  |     |      |  |  |  |  ||   [_      /  \ ||  |  ||     ||  |   __  __  __ ")
    print("\      / |     T|     |\     |l     !|   |   ||     T      |  |  l     !      |  |  |  |  ||     T     \    ||  |  |l     !|  |  |  T|  T|  T")
    print("\_/\_/  l_____jl_____j \____j \___/ l___j___jl_____j      l__j   \___/       l__j  l__j__jl_____j      \___jl__j__j \___/ l__j  l__jl__jl__j")
    print("    "*1000)
    time.sleep(3)
    res = ""
    if counter == 0:
        print("You see a old man at the bar. He has a round, wrinkled, face and happy smile at his face. There's a jazz band playing.")
        time.sleep(3)
        print("Old man: Hey there kiddo, you're a bit young to be at a casino! Oh well, guess you gotta make your fortune somehow!")
        time.sleep(3)
        print("Old man: Well, what's it gonna be?")
        choice = input("Look (l), depart shop (d) ")
        print("      "*1500)
        if choice == "d":
            res = "q"
        if choice == "l":
            print("      "*1500)
            print("You stand on your tiptoes to try to see over the counter...")
            time.sleep(3)
            print("Old man: Huahaha... You need a little help seeing over the counter? I remember when I was a little squirt like you.")
            time.sleep(3)
            print("You struggle to pull up a chair to see over the bar.")
            time.sleep(3)
            print("Old man: It's alright, take your time.")
            time.sleep(3)
            print("You see dusty bottles, bowls of peanuts, a book, and a piano behind the bar.") 
            buy = input("Buy bottle (b), buy peanuts (p), buy book (r), play piano (m), depart shop (d) ")
            time.sleep(0.5)
            if buy == "d":
                res = "q"
            if buy == "b":
                print("      "*1500)
                print("Old man: Oh ho! Unfortuately, you're much too young for that... Want something else?")
                time.sleep(2)
            if buy == "p":
                print("      "*1500)
                print("Old man: You want a snack? You know what, take 'em for free! My treat, kiddo.")
                playsound("sounds/crunch.mp3")
                print("You eat the peanuts.")
            if buy == "r":
                print("      "*1500)
                print("WOULD YOU LIKE TO ENTER, THE SPEED READING CHAMPIONSHIP!!!??????")
                time.sleep(0.5)
                decision = input("Yes (y), no (n) ")
                if decision == "n":
                    print("Oh, okay then.")
                if decision == "y":
                    print("YOU WILL HAVE 30 SECONDS TO READ THE ENTIRE BOOK OF GENESIS")
                    time.sleep(1)
                    print("THE STAKES HAVE NEVER BEEN HIGHER!!!")
                    time.sleep(0.5)
                    print("GET READY ")
                    time.sleep(0.5)
                    print("SET")
                    time.sleep(0.5)
                    print("GOOOOOOOOO!!!!!!!")
                    time.sleep(0.5)
                    print(" INTRODUCTION TO THE OLD TESTAMENT - The New Testament uses various names in referring to the Old Testament: “the Scriptures,” “Holy Scriptures,” “the Writings,” “the Sacred Scriptures,” “the Book,” and “the Sacred Books.” However, God did not deliver to us these sacred book s of the Old Testament canon all at once on a silver platter, as other religions claim to have received theirs. 2 It was a long, historical process that took about ten centuries and involving numerous authors. The canonization of the Holy Scriptures was based upon the foundations laid  by those men of God who walked in the offices of the prophet and the apostle, as Paul reveals in Ephesians 2:20, “And are built upon the foundation of the apostles and prophets, Jesus Christ himself being the chief corner stone;” Church history tells us that God used the office of the prophet to write the Old Testament and the office of the apostle to write the New Testament. Thus, the Old Testament canon was closed when the  prophets of old died. 3 A closer evaluation explains these statements. A. The Old Testament Canon - We read in the Scriptures that the Old Testament custom was to keep holy sheep in the Temple. Moses told the children of Israel to give the Levites the custody of the Sacred Scriptures, saying, “Deuteronomy 17:18, “And it shall be, when he sitteth upon the throne of his kingdom, that he shall write him a copy of this law in a book out of that which is before the priests the Levites:” (Deut 17:18). This custom is practiced up until the time of King Josiah’s reforms when his servants cleaned out the Temple, saying, “And Hilkiah the high priest said unto Shaphan the scribe, I have found the book of the law in the house of the LORD. And Hilkiah gave the book to Shaphan, and he read it.” (2 Kings 22:8) There, hidden in the Temple for decades were the Sacred Scriptures of the Jews. The prophets of the Old Testament were the inspiration for the Jews’ sacred books. The Jews carefully collected these holy prophecies and taught them to their people. There came a time that the prophets ceased to prophesy, and at that point in Jewish history the Old Testament canon was closed. This is confirmed by Josephus, who says, “It is true, our history hath been written since Artaxerxes very particularly, but hath not  been esteemed of the like authority with the former by our forefathers, because there hath not been an exact succession of prophets since that time.” Against A....")
                    playsound("sounds/champion.mp3")
                    print("   "*1000)
                    a = input("Okay! Time's up! How did you do?! To test that you were actually reading, let's see if you know the odd-word out! ")
                    if a == "sheep":
                        print("We have a godlike speedreader among us!!! This is incredible!!! You win......")
                        time.sleep(1)
                        print("A BOWL OF PEANUTS!!!!!!!!!!!!!")
                        time.sleep(1)
                        print("You look dissapointed... I guess I'll eat the peanuts if you don't want them.")
                        print("You hear the crunching of peanuts...")
                        playsound("sounds/crunch.mp3")
                        print("You decide to leave the shop.")
                        time.sleep(2.5)
                        res = "q"
            if choice == "m":
                print("      "*1500)
                print("You decide to play the piano.")
                time.sleep(2)
                print("You realize you don't know how to play the piano. You decide to not play the piano.")
                time.sleep(2)
    


    counter = counter + 1
    if res == "q":
        print("You leave the shop.")
        time.sleep(1.5)
        return counter
    if counter > 0:
        while res != "q":
            print("      "*1500)
            print("You see dusty bottles, bowls of peanuts, a book, and a piano behind the bar.") 
            buy = input("Buy bottle (b), buy peanuts (p), buy book (r), play piano (m), depart shop (d) ")
            time.sleep(0.5)
            if buy == "d":
                print("      "*1500)
                res = "q"
            if buy == "b":
                print("      "*1500)
                print("Old man: Oh ho! Unfortuately, you're much too young for that... Want something else?")
                time.sleep(3)
            if buy == "p":
                print("      "*1500)
                print("Old man: Back at the peanuts again? Here ya go! ")
                playsound("sounds/crunch.mp3")
                print("You eat the peanuts.")
            if buy == "r":
                print("      "*1500)
                print("WOULD YOU LIKE TO ENTER, THE SPEAD READING CHAMPIONSHIP!!!??????")
                time.sleep(0.5)
                decision = input("Yes (y), no (n) ")
                if decision == "n":
                    print("Oh, okay then.")
                    time.sleep(2.5)
                if decision == "y":
                    print("YOU WILL HAVE 30 SECONDS TO READ THE ENTIRE BOOK OF GENESIS")
                    time.sleep(1)
                    print("THE STAKES HAVE NEVER BEEN HIGHER!!!")
                    time.sleep(0.5)
                    print("GET READY ")
                    time.sleep(0.5)
                    print("SET")
                    time.sleep(0.5)
                    print("GOOOOOOOOO!!!!!!!")
                    time.sleep(1.5)
                    print(" INTRODUCTION TO THE OLD TESTAMENT - The New Testament uses various names in referring to the Old Testament: “the Scriptures,” “Holy Scriptures,” “the Writings,” “the Sacred Scriptures,” “the Book,” and “the Sacred Books.” However, God did not deliver to us these sacred book s of the Old Testament canon all at once on a silver platter, as other religions claim to have received theirs. 2 It was a long, historical process that took about ten centuries and involving numerous authors. The canonization of the Holy Scriptures was based upon the foundations laid  by those men of God who walked in the offices of the prophet and the apostle, as Paul reveals in Ephesians 2:20, “And are built upon the foundation of the apostles and prophets, Jesus Christ himself being the chief corner stone;” Church history tells us that God used the office of the prophet to write the Old Testament and the office of the apostle to write the New Testament. Thus, the Old Testament canon was closed when the  prophets of old died. 3 A closer evaluation explains these statements. A. The Old Testament Canon - We read in the Scriptures that the Old Testament custom was to keep holy sheep in the Temple. Moses told the children of Israel to give the Levites the custody of the Sacred Scriptures, saying, “Deuteronomy 17:18, “And it shall be, when he sitteth upon the throne of his kingdom, that he shall write him a copy of this law in a book out of that which is before the priests the Levites:” (Deut 17:18). This custom is practiced up until the time of King Josiah’s reforms when his servants cleaned out the Temple, saying, “And Hilkiah the high priest said unto Shaphan the scribe, I have found the book of the law in the house of the LORD. And Hilkiah gave the book to Shaphan, and he read it.” (2 Kings 22:8) There, hidden in the Temple for decades were the Sacred Scriptures of the Jews. The prophets of the Old Testament were the inspiration for the Jews’ sacred books. The Jews carefully collected these holy prophecies and taught them to their people. There came a time that the prophets ceased to prophesy, and at that point in Jewish history the Old Testament canon was closed. This is confirmed by Josephus, who says, “It is true, our history hath been written since Artaxerxes very particularly, but hath not  been esteemed of the like authority with the former by our forefathers, because there hath not been an exact succession of prophets since that time.” Against A....")
                    playsound("sounds/champion.mp3")
                    print("   "*1000)
                    a = input("Okay! Time's up! How did you do?! To test that you were actually reading, let's see if you know the odd-word out! ")
                    if a == "sheep":
                        print("We have a godlike speedreader among us!!! This is incredible!!! You win......")
                        time.sleep(3)
                        print("A BOWL OF PEANUTS!!!!!!!!!!!!!")
                        time.sleep(3)
                        print("You look dissapointed... I guess I'll eat the peanuts if you don't want them.")
                        time.sleep(3)
                        print("You hear the crunching of peanuts...")
                        playsound("sounds/crunch.mp3")
                        print("You decide to leave the shop.")
                        time.sleep(2)
                        res = "q"
            if buy == "m":
                print("             "*1000)
                print("You decide to play the piano.")
                time.sleep(3)
                print("You realize you don't know how to play the piano. You decide to not play the piano.")
                time.sleep(3)
    playsound("sounds/magic.mp3")

                
            
            
            
            

        
        
        


def layout(hand, current_bet, total_money):
    print(
        " ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** "
    )
    print(
        "                                                                                                                            "
    )
    print(
        "                                                                                                                            "
    )
    print(
        "               .------..------..------.                                |--Key-------|                                       "
    )
    print(
        "               |!.--. ||!.--. ||!.--. |                                |            |                                       "
    )
    print(
        "               | :/\: || :/\: || :/\: |                                | Hit is h   |                                       "
    )
    print(
        "               | (__) || :\/: || (__) |                                |            |                                       "
    )
    print(
        "               | '--'A|| '--'A|| '--'Z|                                | Stay is s  |                                       "
    )
    print(
        "               `------'`------'`------'                                |            |                                       "
    )
    print(
        "                                                                       | Shop is $  |                                       "
    )
    print(
        "                                                                       |            |                                       "
    )
    print(
        "                                                                       | Leave is q |                                       "
    )
    print(
        "                                                                       |____________|                                      "
    )
    print(
        f" **************         --Statistics--       ************                                                                  "
    )
    print(
        f" ** __________________________________________________ **                                                                "
    )
    print(
        f" **|                                                  |**                                                               "
    )
    print(
        f" **| Hand: {hand}                                                                                                        "
    )
    print(
        f" **|                                                  |**                                                                "
    )
    print(
        f" **| Hand total: {return_hand_value(hand)}                                                                               "
    )
    print(
        f" **| Current bet: {current_bet}                                                                                                            "
    )
    print(
        f" **| Total money: {total_money}                                                                                 "
    )
    print(
        f" **|__________________________________________________|**                                                             "
    )


def remove_card_from_deck(card, deck):
    # Removes passed card from deck and returns None.
    deck[card[0]].remove(card[1])


def add_card_to_hand(card, hand):
    # Adds card to hand and returns hand and calls remove_card_from_deck to remove it from the deck.
    # card ex. ["suit", number]
    hand.append(card)


def get_card_value(card):
    """
    Returns card value as an integer.
    Ex. "D1" or "SJ" or "H10"
    """
    if card[1] in "JQK":
        return 10
    if card[1] in "A":
        return 0
    return int(card[1:])


def return_hand_value(hand):
    total = 0
    ace_counter = 0
    for card in hand:
        if card == "SA" or card == "CA" or card == "DA" or card == "HA":
            ace_counter = ace_counter + 1
        total = total + get_card_value(card)
    if ace_counter > 0:
        big_aces = ace_counter * 11
        if total + big_aces > 21:
            total = total + ace_counter
        else:
            total = total + big_aces
    return total


def set_up():
    print_casino()


def print_casino():
    print("        " * 1000)
    print(
        "                                                                                                                             "
    )
    print(
        "                                                                                                                             "
    )
    print(
        "        CCCCCCCCCCCCC               AAA                 SSSSSSSSSSSSSSS IIIIIIIIIINNNNNNNN        NNNNNNNN     OOOOOOOOO    "
    )
    print(
        "     CCC::::::::::::C              A:::A              SS:::::::::::::::SI::::::::IN:::::::N       N::::::N   OO:::::::::OO   "
    )
    print(
        "   CC:::::::::::::::C             A:::::A            S:::::SSSSSS::::::SI::::::::IN::::::::N      N::::::N OO:::::::::::::OO "
    )
    print(
        "  C:::::CCCCCCCC::::C            A:::::::A           S:::::S     SSSSSSSII::::::IIN:::::::::N     N::::::NO:::::::OOO:::::::O"
    )
    print(
        " C:::::C       CCCCCC           A:::::::::A          S:::::S              I::::I  N::::::::::N    N::::::NO::::::O   O::::::O"
    )
    print(
        "C:::::C                        A:::::A:::::A         S:::::S              I::::I  N:::::::::::N   N::::::NO:::::O     O:::::O"
    )
    print(
        "C:::::C                       A:::::A A:::::A         S::::SSSS           I::::I  N:::::::N::::N  N::::::NO:::::O     O:::::O"
    )
    print(
        "C:::::C                      A:::::A   A:::::A         SS::::::SSSSS      I::::I  N::::::N N::::N N::::::NO:::::O     O:::::O"
    )
    print(
        "C:::::C                     A:::::A     A:::::A          SSS::::::::SS    I::::I  N::::::N  N::::N:::::::NO:::::O     O:::::O"
    )
    print(
        "C:::::C                    A:::::AAAAAAAAA:::::A            SSSSSS::::S   I::::I  N::::::N   N:::::::::::NO:::::O     O:::::O"
    )
    print(
        "C:::::C                   A:::::::::::::::::::::A                S:::::S  I::::I  N::::::N    N::::::::::NO:::::O     O:::::O"
    )
    print(
        " C:::::C       CCCCCC    A:::::AAAAAAAAAAAAA:::::A               S:::::S  I::::I  N::::::N     N:::::::::NO::::::O   O::::::O"
    )
    print(
        "  C:::::CCCCCCCC::::C   A:::::A             A:::::A  SSSSSSS     S:::::SII::::::IIN::::::N      N::::::::NO:::::::OOO:::::::O"
    )
    print(
        "   CC:::::::::::::::C  A:::::A               A:::::A S::::::SSSSSS:::::SI::::::::IN::::::N       N:::::::N OO:::::::::::::OO "
    )
    print(
        "     CCC::::::::::::C A:::::A                 A:::::AS:::::::::::::::SS I::::::::IN::::::N        N::::::N   OO:::::::::OO   "
    )
    print(
        "        CCCCCCCCCCCCCAAAAAAA                   AAAAAAASSSSSSSSSSSSSSS   IIIIIIIIIINNNNNNNN         NNNNNNN     OOOOOOOOO     "
    )
    print("     " * 250)
    input("Press enter to continue. ")


def decide_winner(hand_value, computer_hand_value):
    if hand_value == computer_hand_value:
        return "draw"
    if hand_value > 21:
        return "c"
    if computer_hand_value > 21:
        return "h"
    if computer_hand_value == hand_value:
        raise Exception("Not implemented yet.")
    if computer_hand_value > hand_value:
        return "c"
    else:
        return "h"


def create_computer_hand(hand, computer_hand, deck):
    while return_hand_value(computer_hand) < return_hand_value(hand):
        add_card_to_hand(deck.pop(), computer_hand)


def print_board(hand, current_bet, total_money):
    print("      " * 500)
    layout(hand, current_bet, total_money)


def clear_stats(hand, computer_hand, deck, og_deck):
    hand.clear()
    computer_hand.clear()
    deck["hearts"] = []
    deck["diamonds"] = []
    deck["spades"] = []
    deck[item] = og_deck[item]


def get_deck():
    deck = [
        "HA",
        "H2",
        "H3",
        "H4",
        "H5",
        "H6",
        "H7",
        "H8",
        "H9",
        "H10",
        "HJ",
        "HQ",
        "HK",
        "DA",
        "D2",
        "D3",
        "D4",
        "D5",
        "D6",
        "D7",
        "D8",
        "D9",
        "D10",
        "DJ",
        "DQ",
        "DK",
        "SA",
        "S2",
        "S3",
        "S4",
        "S5",
        "S6",
        "S7",
        "S8",
        "S9",
        "S10",
        "SJ",
        "SQ",
        "SK",
        "CA",
        "C2",
        "C3",
        "C4",
        "C5",
        "C6",
        "C7",
        "C8",
        "C9",
        "C10",
        "CJ",
        "CQ",
        "CK",
    ]
    random.shuffle(deck)
    return deck


def print_winner(res):
    if res == "h":
        print("   ")
        print("YOU WON THE GAME!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")
    elif res == "c":
        print("   ")
        print("you lost the game :(")
    elif res == "draw":
        print("   ")
        print("You drew the hand! Play another hand!")
    input("Press enter to continue. ")

def make_bet(current_bet, is_bet_valid, total_money, fn_input=input):
    def is_bet_number(bet):
        numbers = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "0"]
        for item in bet:
            if item not in numbers:
                return False
        return True
    while is_bet_valid is False:
        current_bet = fn_input(f" Total money - {total_money} | How much would you like to bet? ")
        if is_bet_number(current_bet) is True and int(current_bet) < total_money:
            current_bet = int(current_bet)
            return current_bet


def play_hand(deck, total_money, shop_counter, fn_input=input, is_print=True):
    """
    Possible outputs:
    "q" - quit hand and game
    "draw" - no winner
    "h" - human player won
    "c" - computer won
    """
    hand = []
    computer_hand = []
    add_card_to_hand(deck.pop(), hand)
    add_card_to_hand(deck.pop(), hand)
    char = ""
    is_bet_valid = False
    current_bet = None
    current_bet = make_bet(current_bet, is_bet_valid, total_money)
    while True:
        if is_print:
            print_board(hand, current_bet, total_money)
        if return_hand_value(hand) > 21:
            return "c", current_bet, shop_counter

        char = fn_input("Would you like to hit (h)? Or stay (s)? Or quit (q)? Or shop ($) ")
        if char == "q":
            return "q", current_bet, shop_counter
        if char == "$":
            shop(shop_counter)
            shop_counter = shop_counter + 1
        elif char == "h":
            add_card_to_hand(deck.pop(), hand)
        elif char == "s":
            create_computer_hand(hand, computer_hand, deck)
            winner = decide_winner(return_hand_value(hand), return_hand_value(computer_hand))
            return winner, current_bet, shop_counter


def adjust_money(current_bet, total_money, winner):
    if winner == "h":
        total_money = total_money + current_bet
    if winner == "c":
        total_money = total_money - current_bet
    return total_money


def play_game(deck, total_money, shop_counter, fn_play_hand=play_hand, is_print=True):
    """
    Possible outputs:
    "h" - human won
    "c" - computer won
    "q" - quit game
    """
    res = ""
    current_bet = ""
    while res in ["", "draw"]:
        res, current_bet, shop_counter = fn_play_hand(deck, total_money, shop_counter)
        if is_print:
            print_winner(res)
    return res, current_bet, shop_counter


def main(fn_play_game=play_game, is_print=True):
    res = ""
    current_bet = ""
    total_money = 637
    shop_counter = 0
    if is_print:
        set_up()
    while res in ["c", "h", ""]:
        deck = get_deck()
        res, current_bet, shop_counter = fn_play_game(deck, total_money, shop_counter)
        total_money = adjust_money(current_bet, total_money, res)
        if res == "q":
            music_yes = input("You see the old man come up to you. \n Old man: Before you go, wanna hear some music? I composed it myself. (y) (n) ")
            if music_yes == "y":
                playsound("sounds/bunny.mp3")
            return "quit"

    # clear_stats(hand, computer_hand, deck, og_deck)


if __name__ == "__main__":
    main()
