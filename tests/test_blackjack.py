import pytest
from blackjack import (
    add_card_to_hand,
    clear_stats,
    create_computer_hand,
    decide_winner,
    main,
    play_game,
    play_hand,
    return_hand_value,
)


@pytest.mark.skip(reason="")
def test_clear_stats():
    hand = [1, 2, 3, 4]
    computer_hand = [5, 6, 7, 8]
    deck = {"a": 1, "b": 2, "c": 3, "d": 4}
    og_deck = {"a": 1, "b": 2, "c": 3}
    clear_stats(hand, computer_hand, deck, og_deck)
    assert hand == []
    assert computer_hand == []
    assert deck == og_deck


def test_decide_winner_draw():
    res = decide_winner(17, 17)
    assert res == "draw"


def test_decide_winner_human():
    res = decide_winner(20, 17)
    assert res == "h"


def test_decide_winner_computer():
    res = decide_winner(16, 20)
    assert res == "c"


def test_empty_hand_value():
    res = return_hand_value([])
    assert res == 0


def test_hand_value():
    res = return_hand_value(["HK", "H10"])
    assert res == 20


def test_add_card_to_empty_hand():
    hand = []
    add_card_to_hand("H7", hand)
    assert hand == ["H7"]


def test_add_card_to_hand():
    hand = ["H6"]
    add_card_to_hand("H7", hand)
    assert hand == ["H6", "H7"]


def test_add_face_card_to_hand():
    hand = ["H10"]
    add_card_to_hand("HJ", hand)
    assert hand == ["H10", "HJ"]


def test_play_hand_stay_right_away():
    def mock_input(_):
        return inputs.pop()

    inputs = ["s"]
    deck = ["C1", "S9", "D5", "C10", "C9", "C8", "C7", "C5"]
    assert play_hand(deck, mock_input, False) == "c"


def test_play_hand_h():
    def mock_input(_):
        return inputs.pop()

    inputs = [
        "s",
        "h",
    ]
    deck = ["C1", "S9", "D5", "C10", "C9", "C8", "C7", "C5"]
    assert play_hand(deck, mock_input, False) == "h"


def test_play_hand_c():
    def mock_input(_):
        return inputs.pop()

    inputs = [
        "s",
        "h",
    ]
    deck = ["C1", "S9", "D2", "C10", "C9", "C8", "C7", "C5"]
    assert play_hand(deck, mock_input, False) == "c"


def test_play_hand_draw():
    def mock_input(_):
        return inputs.pop()

    inputs = [
        "s",
        "h",
    ]
    deck = ["C1", "S9", "D1", "C10", "C9", "C8", "C7", "C5"]
    assert play_hand(deck, mock_input, False) == "draw"


def test_play_hand_over_21():
    def mock_input(_):
        return inputs.pop()

    inputs = [
        "s",
        "h",
    ]
    deck = ["H10", "C9", "S10", "D10", "C10"]
    assert play_hand(deck, mock_input, False) == "c"
    assert deck == ["H10", "C9"]


def test_play_hand_quit():
    def mock_input(_):
        return inputs.pop()

    inputs = [
        "s",
        "q",
    ]
    deck = ["C1", "S9", "D1", "C10", "C9", "C8", "C7", "C5"]
    assert play_hand(deck, mock_input, False) == "q"


def test_play_game_win():
    def mock_play_hand(_):
        return play_hand_res.pop()

    play_hand_res = ["c", "c", "h"]
    res = play_game(None, mock_play_hand, is_print=False)
    assert res == "h"


def test_play_gamew_lose():
    def mock_play_hand(_):
        return play_hand_res.pop()

    play_hand_res = ["c", "c", "c"]
    res = play_game(None, mock_play_hand, is_print=False)
    assert res == "c"


def test_play_game_draw_win():
    def mock_play_hand(_):
        return play_hand_res.pop()

    play_hand_res = ["c", "c", "h", "draw"]
    res = play_game(None, mock_play_hand, is_print=False)
    assert res == "h"


def test_play_game_draw_lose():
    def mock_play_hand(_):
        return play_hand_res.pop()

    play_hand_res = ["c", "c", "c", "draw"]
    res = play_game(None, mock_play_hand, is_print=False)
    assert res == "c"


def test_main_quit():
    def mock_play_game(_):
        return "q"

    res = main(mock_play_game, False)
    assert res == "quit"


def test_game_1():
    deck = ["C1", "S9", "D5", "C10", "C9", "CJ", "CQ", "CK"]
    hand = []
    computer_hand = []
    add_card_to_hand(deck.pop(), hand)
    add_card_to_hand(deck.pop(), hand)
    create_computer_hand(hand, computer_hand, deck)
    winner = decide_winner(return_hand_value(hand), return_hand_value(computer_hand))
    assert winner == "h"
